from random import randint

#Whats your name?
name = str(input("Hey whats your name? "))

#Find what month/person was born
month_number = randint(1,12)
year_number = randint(1920, 2022)

#counter
counter = 0
while (counter < 5):
    keep_guessing = "Guess {}: {}, were you born in, {} / {}?"
    print(keep_guessing.format(counter, name, month_number, year_number))
    response = input(str("yes or no")).lower()
    if response == "yes":
        print("I knew it")
        counter == 5
        exit()
    else:
        print("Drat! Lemme try again!")
        counter += 1
print("I have other things to do!")
